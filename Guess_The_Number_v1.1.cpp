#include <iostream>
using namespace std;
#include <cstdlib>
#include <ctime>
#include <conio.h>

int main()
{
    srand( time( NULL ) );

    int liczba = ( rand() % 10 ) + 1;
    int wpisana_liczba;
    int proby;

    proby = 0;


    cout << "Witam w mojej prostej grze C++.\n   Zostala stworzona liczba pomiedzy 1 a 10, sprobuj ja trafic\n\n\n" << endl;
    do
    {
        cout << "Twoja liczba: ";

        cin >> wpisana_liczba;
        if( wpisana_liczba < 0, wpisana_liczba > 10)
        {
            proby++;
            cout << "Wpisz poprawna liczbe\n";
        }

        if( wpisana_liczba == liczba )
        {
            proby++;
            cout << "BRAWO WYGRALES!!!!!!!\n\n WPISANA LICZBA TO: " << liczba << endl;
            cout << "\n\nIlosc prob: " << proby << endl;
        }
        if( wpisana_liczba != liczba )
        {
            proby++;
            cout << "Niestety, przegrales :(\n\n    =Sprobuj ponownie= " << endl;
        }
    } while( wpisana_liczba != liczba );

    getch();

    return 0;
}
