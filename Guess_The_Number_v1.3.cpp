#include <iostream>
using namespace std;
#include <cstdlib>
#include <ctime>
#include <conio.h>

int main()
{
    srand( time( NULL ) );

    int basen = ( rand() % 50 ) + 1;
    int wpisana_liczba;
    int proby;
    bool again = 1;

    proby = 0;


    cout << "Welcome in my simple C++ game\n   A number had been picked between 1 and 50, try to guess it. \n   You have 25 chances\n\n\n" << endl;

    do{

        do{
            cout << "Your number: ";

            cin >> wpisana_liczba;


            if( wpisana_liczba == basen ){
                proby++;
                cout << "\n\n VICTORY!!!!!!!\n\n PICKED NUMBER IS: " << basen << endl;
                cout << "\n\nNumber of tries: " << proby << endl;
            }

            if( proby > 25){
                cout << "YOU LOST!!!\n\n You have lost your chances. Picked number is: " << basen << endl;
                break;
            }

            if( wpisana_liczba != basen ){
                if( wpisana_liczba < 0, wpisana_liczba > 50) {
                    proby++;
                    cout << "\n\nEnter correct number\n";
                }else{
                    proby++;
                    cout << "Try Again\n\n " << endl;
                }
            }
        } while( wpisana_liczba != basen );

        cout <<"\n\n\n\nPlay again? [1/0]" << endl;
        cin >> again;
    }while(again == 1);

    getch();

    return 0;
}
