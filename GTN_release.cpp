#include <iostream>
using namespace std;
#include <cstdlib>
#include <ctime>
#include <conio.h>

int generator(int difficulty = 1, string response = ""){
    srand( time( NULL ) );
    int end;

    switch(difficulty){
        case 1:
            end = 15;
            break;
        case 2:
            end = 20;
            break;
        case 3:
            end = 30;
            break;
        case 4:
            end = 40;
            break;
        case 5:
            end = 50;
            break;
        default:
            return 0;
    }


    int random = ((rand() % end ) + 1);

    if(response == "random"){
        return random;
    }

    if(response == "end"){
        return end;
    }
}


int main() {

    int difficulty, tries, input, finish, random, end;

    string choose_difficulty =  "\n++++++++++++++++++++++++++++++++++++++\n Please choose the range between which the number is to be generated:\n\t[1]: 1 - 15\n\t[2]: 1 - 20\n\t[3]: 1 - 30\n\t[4]: 1 - 40\n\t[5]: 1 - 50\n\t[0]: QUIT GAME\n++++++++++++++++++++++++++++++++++++++";
    string difficulty_text= " \n\n Difficulty:  ";
    string play_again = "Would you like to play again?\n\n  [0] Quit\n  [1] Play Again.\nYour Response: ";

    cout << "Welcome to  my C++ game, created by Wojtek Bednarzak. \n This is v 2.0 which is a complete rewrite of the code, adding more \n functionality and fixing serious bugs from the previous release.  \n\n ==GUESS THE NUMBER== \n\nInstructions:\n\tSelect Your Difficulty\n\tEnter a number in your difficulty range\n\tYou have 10 tries. Try to Guess it before your tries run out!!!" << endl;

    do{

        tries = 0;

        cout << choose_difficulty << difficulty_text;
        cin >> difficulty;

        while( (difficulty < 0) || (difficulty > 5) ){
            if(!cin){
                cin.clear();
                cout << "Please input a number between 0 and 5" <<  difficulty_text <<endl;
                cin >> difficulty;
            } else {
                cout << "Please input a number between 0 and 5" <<  difficulty_text <<endl;
                cin >> difficulty;
            }

        }

        if(difficulty == 0){
            break;
        }

        random = generator(difficulty, "random");
        end = generator(difficulty, "end");

        //cout << "===DEBUG===\tGenerated Number: " << random << "\n\n";
        cout << "Please input a number between 0 and " << end << "\n";

        do{
            cout << "Your Number:  ";
            cin >> input;

            if(input == 0){
                break;
            }

            if( (input < 0) || (input > end) ){
                cout << "Please input a number between 0 and " << end << "\n";
            }


            if(input == random){
                cout << "\n=============================\n\n\t  You Won!!!\n\n=============================\n";
                cout << "\n     Generated Number: " << random << " \n\n=============================\n";
                break;
            } else {
                tries++;
            }
        }while(tries < 10);

        if(tries > 9){
            cout << "\n=============================\n\n\t You lost :(\n\n=============================\n";
            cout << "\n     Generated Number: " << random << " \n\n=============================\n";
        }

        cout << play_again;
        cin >> finish;



    }while(finish == 1 );

    return 0;
}
